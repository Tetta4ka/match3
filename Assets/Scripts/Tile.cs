﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public class Tile : MonoBehaviour {
	private static Vector2 prevPos = new Vector2 (-1, 0);
	private bool isSelected = false;
    public int id;

    void OnMouseDown()
    {
        if (Game.instance.lockBoard) return;

        if (isSelected) { 
            deselect();
        } 
        else {
			Vector2 pos = Game.instance.tiles.Where(x => x.Value == this).First().Key;

			if (prevPos == new Vector2(-1, 0)) {   
                select(pos);
            } 
            else {
                bool flag = false;

				for (int i = 0; i < Game.instance.directions.Length; i++){

					if (pos + Game.instance.directions[i] == prevPos) flag = true;
                }
               
                if (flag) {
					StartCoroutine(Game.instance.swapUser(pos, prevPos));
                }
                else {
					Game.instance.tiles[prevPos].deselect();
                    select(pos);
                }
            }
        }
    }



	public void select(Vector2 pos) {
		isSelected = true;
		Game.instance.selector.transform.position = transform.position;
        prevPos = pos;
	}

	public void deselect() {
		isSelected = false;
		Game.instance.selector.transform.position = new Vector3(-100, 0,0);
		prevPos = new Vector2(-1 , 0);
	}

}