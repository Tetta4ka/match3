﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;


public class Game : MonoBehaviour
{
	public static Game instance;
	public List<Sprite> sprites = new List<Sprite>();
	public GameObject tile;
	public GameObject selector;
	public GameObject bg1;
	public GameObject bg2;
	public Text score;
	public GameObject shuffleLabel;
	public int xSize, ySize;

	public Dictionary<Vector2, Tile> tiles = new Dictionary<Vector2, Tile>();
	public bool lockBoard = false;
	public bool isMatching = false;
	public int matchingCount = 0;
	public Vector2[] directions = new Vector2[] { new Vector2(0, 1), new Vector2(0, -1), new Vector2(-1, 0), new Vector2(1, 0) };

	private float tileSize = 1f;
	private float delayMoving = 0.03f;
	public List<Vector2> checkingPosForShuffle = new List <Vector2>();

	void Start()
	{
		instance = GetComponent<Game>();
		correctBoardTransform();
		createBoard();
	}

	private void createBoard()
	{
		int[] leftId = new int[ySize];
		int downId = 0;
		int id = 0;

		List<int> possibleIds = new List<int>();
		for (int i = 0; i < sprites.Count; i++) possibleIds.Add(i);
		List<Vector2> checkingPos = new List<Vector2>();

		for (int x = 0; x < xSize; x++) {
			for (int y = 0; y < ySize; y++) {
				GameObject newTile = Instantiate(tile, new Vector3(0, 0, 0), tile.transform.rotation);
				Vector2 pos = new Vector2(x, y);
				tiles[pos] = newTile.GetComponent<Tile>();

				newTile.transform.parent = transform;
				newTile.transform.localPosition = new Vector3(tileSize * x, tileSize * y);

				possibleIds.Remove(leftId[y]);
				if (leftId[y] != downId) possibleIds.Remove(downId);
				id = possibleIds[Random.Range(0, possibleIds.Count)];

				newTile.GetComponent<SpriteRenderer>().sprite = sprites[id];
				tiles[pos].id = id;
				checkingPos.Add(pos);
				possibleIds.Add(leftId[y]);
				if (leftId[y] != downId) possibleIds.Add(downId);
				leftId[y] = id;
				downId = id;
			}
		}
		if (!checkPossibleMatching(checkingPos)) StartCoroutine( shuffleBoard());
	}

	public bool checkPossibleMatching (List<Vector2> checkingPos)
	{
		Vector2 pos = new Vector2 (0, 0);
		List<Vector2> tempMatchingTiles = new List<Vector2> ();
		bool isPossibleMatching = false;
		for (int e = 0; e < checkingPos.Count; e++) {
			pos = checkingPos [e];
			for (int i = 0; i < directions.Length; i++) {
				pos = pos + directions [i];

				if (posInBoard (pos)) {
					swap (checkingPos [e], pos);
					for (int dd = 0; dd < 3; dd += 2) {
						tempMatchingTiles = findMatch (checkingPos [e], new Vector2 [] { directions [dd], directions [dd + 1] });
						if (tempMatchingTiles.Count > 2) isPossibleMatching = true;
						tempMatchingTiles = findMatch(pos, new Vector2[] { directions[dd], directions[dd + 1] });
						if (tempMatchingTiles.Count > 2) isPossibleMatching = true;

					}
					swap(pos, checkingPos[e]);
					if (isPossibleMatching) return true;
				}
			}
		}


		return false;

	}


    public IEnumerator swapUser(Vector2 pos, Vector2 prevPos)
	{
		lockBoard = true;
		matchingCount = 0;
		swap(pos, prevPos);

		tiles[pos].deselect();
		checkingPosForShuffle.Clear();

		yield return updateTransformTiles(new List<Vector2>() { prevPos, pos }, new List<Vector2>() { pos, prevPos });
		yield return StartCoroutine(checkMatch(new Vector2[2] { pos, prevPos }));

		if (matchingCount == 0) {
			swap(prevPos, pos);
			yield return updateTransformTiles(new List<Vector2>() { prevPos, pos }, new List<Vector2>() { pos, prevPos });
		} else {
			score.text = (int.Parse(score.text) + matchingCount * 5 - 5).ToString();

		}
		matchingCount = 0;
		lockBoard = false;

	}

	private IEnumerator checkMatch(Vector2[] checkingPos)
	{

		bool[,] matchingTiles = findMatchAll(checkingPos);
		if (matchingCount == 0) yield return false;
		List<Vector2> nextCheckingPos = new List<Vector2>();
		List<Vector2> from = new List<Vector2>();

		Dictionary<Vector2, Tile> newTiles = new Dictionary<Vector2, Tile>();
		int currentY = 0;
		int outY = 0;

		for (int xx = 0; xx < xSize; xx++) {
			currentY = 0;
			for (int yy = 0; yy < ySize; yy++) {
				Vector2 pos1 = new Vector2(xx, yy);
				Vector2 pos2 = new Vector2(xx, currentY);

				if (!matchingTiles[xx, yy]) {
					newTiles[pos2] = tiles[pos1];
					if (yy != currentY) {
						nextCheckingPos.Add(pos2);
						from.Add(pos1);
						//if (!checkingPosForShuffle.Contains(pos2)) checkingPosForShuffle.Add(pos2);

					}
					currentY++;
				}
			}
			outY = 0;
			for (int yy = 0; yy < ySize; yy++) {
				if (matchingTiles[xx, yy]) {
					Vector2 pos1 = new Vector2(xx, yy);
					Vector2 pos2 = new Vector2(xx, currentY);

					nextCheckingPos.Add(pos2);
					from.Add(new Vector2(xx, outY + ySize));

					newTiles[pos2] = tiles[pos1];
					int newId = Random.Range(0, sprites.Count);
					Sprite sprite = sprites[newId];
					newTiles[pos2].id = newId;
					newTiles[pos2].GetComponent<SpriteRenderer>().sprite = sprite;
					outY++;
					currentY++;
					//if (!checkingPosForShuffle.Contains(pos2)) checkingPosForShuffle.Add(pos2);
				}

			}
		}
		tiles = newTiles;

		if (from.Count > 0) {
			yield return updateTransformTiles(from, nextCheckingPos);
			yield return StartCoroutine(checkMatch(nextCheckingPos.ToArray()));
			if (!Game.instance.checkPossibleMatching(tiles.Keys.ToList())) yield return StartCoroutine(Game.instance.shuffleBoard());

		}
	}

	private bool[,] findMatchAll(Vector2[] checkingPos)
	{
		var checkingId = 0;
		List<Vector2> tempMatchingTiles = new List<Vector2>();
		bool[,] matchingTiles = new bool[Game.instance.xSize, Game.instance.ySize];

		for (int e = 0; e < checkingPos.Length; e++) {
			checkingId = Game.instance.tiles[checkingPos[e]].id;

			for (int dd = 0; dd < 3; dd += 2) {
				tempMatchingTiles = findMatch(checkingPos[e], new Vector2[] { directions[dd], directions[dd + 1] });
				if (tempMatchingTiles.Count > 2) {

					foreach (Vector2 vv in tempMatchingTiles) {
						if (!matchingTiles[(int)vv.x, (int)vv.y]) matchingCount++;
						matchingTiles[(int)vv.x, (int)vv.y] = true;

					}
				}

			}
		}
		return matchingTiles;
	}

	private List<Vector2> findMatch (Vector2 pos, Vector2 [] directions)
	{
		List<Vector2> matchingTiles = new List<Vector2>();
		int checkingId = Game.instance.tiles [pos].id;
		bool flag = true;
		Vector2 checkingPos = pos;
		matchingTiles.Add(checkingPos);
		for (int i = 0; i < directions.Length; i++) {
			flag = true;
			checkingPos = pos;

			while (flag) {
				checkingPos += directions [i];

				if (posInBoard(checkingPos) && checkingId == Game.instance.tiles [checkingPos].id) {
					matchingTiles.Add (checkingPos);
				} else {
					flag = false;
				}
			}

		}
		return matchingTiles;
	}

	public bool posInBoard (Vector2 pos)
	{
		return pos.x < xSize && pos.y < ySize && pos.x >= 0 && pos.y >= 0;
	}

	public void swap (Vector2 pos1, Vector2 pos2)
	{
		Tile tile;
		tile = tiles [pos1];
		tiles [pos1] = tiles [pos2];
		tiles [pos2] = tile;
	}

	public IEnumerator updateTransformTiles (List<Vector2> from, List<Vector2> to)
	{
		bool flag = true;
		float step = 0.2f;
		float k = 0;
		Vector3 move = Vector3.zero;
		float x = 0;
		float y = 0;
		while (flag) {
			flag = false;
			for (int i = 0; i < to.Count; i++) {
				var n = (to[i] - from [i]).normalized;
				x = k * n.x + from [i].x;
				y = k * n.y + from [i].y;

				if ((x - to [i].x) * n.x < -0.1f || (y - to [i].y) * n.y < -0.1f) {
					tiles [to [i]].transform.localPosition =
					    new Vector3 (x + step * n.x, y + step * n.y) * tileSize;
					flag = true;
				}
			}
			yield return new WaitForSeconds (delayMoving);
			k += step;
		}


	}


	public IEnumerator shuffleBoard()
	{
		lockBoard = true;
		shuffleLabel.SetActive(true);
		yield return new WaitForSeconds(0.5f);
		System.Random rand = new System.Random();
		float step = 0.1f;
		Dictionary<Vector2, int> tempDict = new Dictionary<Vector2, int>();
		List<Tile> randomList = tiles.Values.OrderBy(x => rand.Next()).ToList();
		int i = 0;
		foreach (Vector2 pos in tiles.Keys) {
			tempDict[pos] = randomList[i].id;

			i++;
		}
		for (float j = 1; j < ySize +2 - step; j = j + step) {
			foreach (Vector2 pos in tempDict.Keys) {
				tiles[pos].transform.localPosition = new Vector3(pos.x * tileSize, (tiles[pos].transform.localPosition.y + step) * tileSize);
				if (Mathf.Abs(-tiles[pos].transform.localPosition.y + ySize * tileSize) <= 0.001f) {
					tiles[pos].transform.localPosition = new Vector3(pos.x * tileSize, -1 * tileSize);
					tiles[pos].GetComponent<SpriteRenderer>().sprite = sprites[tempDict[pos]];
					tiles[pos].id = tempDict[pos];
				}


			}
			yield return new WaitForSeconds(0.02f);

		}

		yield return StartCoroutine(checkMatch(tiles.Keys.ToArray()));
		if (!checkPossibleMatching(tiles.Keys.ToList())) yield return shuffleBoard();
		shuffleLabel.SetActive(false);

		lockBoard = false;

	}

	public void correctBoardTransform()
	{
		transform.position = new Vector3((tileSize - xSize) * 0.5f, (tileSize - ySize) * 0.5f, 0);
		bg1.transform.position = new Vector3(0, 2.5f + ySize * tileSize * 0.5f);
		bg2.transform.position = new Vector3(0, -2.5f  -ySize * tileSize *0.5f);

	}
}


/*
public struct Pos
{
	public int x, y;
	public Pos(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	public static Pos operator +( Pos pos1,  Pos pos2){
		return new Pos(pos1.x + pos2.x, pos1.y + pos2.y);
	}

}
*/
